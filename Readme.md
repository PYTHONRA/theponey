## Annuaire interne de l'association

# Création de compte utilisateur

Chaque membre dispose d'un numéro d'adhérent qu'il doit renseigner pour pouvoir créer son compte. Ces numéros contiennent tous la suite de caractère suivante : adh-2045-. Il faut donc vérifier lors de la création du compte que le numéro transmit par l'utilisateur contienne bien cette suite de caractères et qu'elle n'est pas déjà présente en base de données.

# Connexion et déconnexion

Lorsqu'un utilisateur arrive sur le site, il ne peut consulter que la page de connexion (et/ou de création de compte). Le formulaire de connexion est tout à fait classique (login/mot de passe).

# Gestion du profil

À la première connexion, l'utilisateur sera invité à renseigner ses informations de profil. L'utilisateur doit alors choisir 3 à 8 centre d'intérêts parmi une liste prédéfinie, fournie en annexe 1. Il ne doit pas pouvoir poster son formulaire tant que cette contrainte n'est pas respectée.
L'utilisateur peut choisir d'uploader (téléverser) une image de profil qui ne doit pas faire plus de 1Mo et être de type PNG, JPG ou GIF. L'utilisateur n'est pas obligé de fournir une image.

# Affichage de la liste des adhérents (sécurisé)

Cette page affiche tous les membres inscrits. Il s'agit ici d'afficher pour chaque membre :

- son pseudo,
- son prénom,
  depuis combien de jours l'utilisateur est adhérent
  Les membres n'ayant pas encore renseigné leur profil doivent apparaitre en grisé alors que les autres apparaissent en "clair" et leur profil est consultable lorsque l'on clique dessus

# Recherche de membres par nom (sécurisé)

Il faut pouvoir rechercher de façon insensible à la casse (indifféremment avec majuscules, minuscules ou les deux) un utilisateur par son nom, son pseudo, son login ou son email. Cette fonctionnalité est disponible lorsque l'utilisateur se trouve sur la liste des membres exclusivement.

# Recherche de membres par centre d'intérêt (sécurisé)

L'utilisateur peut aussi sélectionner un centre d'intérêt dans une liste. La recherche lui retourner alors toutes les personnes qui ont choisi ce centre d'intérêt.

La liste d'intérêt devrait être générée dynamiquement en fonction de la liste présente en base

Les recherches sont mutuellement exclusives : soit on recherche par nom, soit par centre d'intérêt.
