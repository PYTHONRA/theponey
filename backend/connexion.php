<?php

include('header.php');
include_once('bdd.php'); // j'inclus le fichier de connexion de la bdd
//Je recupere les données du formulaire
$pseudo = $_POST['pseudo']; //les clés c'est dans le formulaire name
$email = $_POST['email'];
$password = $_POST['password'];

$requete = "SELECT adherentID, pseudo, email, nom, prenom, password FROM adherents WHERE pseudo=:input OR email=:input OR numero=:input LIMIT 1";

try { 
    $requetePreparee = $connexion->prepare($requete);
    $requetePreparee->bindParam("input", $pseudo); //paramettre de input variable pseudo
    $requetePreparee->execute(); 
    $resultat = $requetePreparee->fetch(PDO::FETCH_ASSOC);//sous forme de tableau
} catch (Exception $err) {
    http_response_code(500);
    echo json_encode($err->getMessage());
    exit;
}
$hash = $resultat['password']; 

if(password_verify($password, $hash)) { // on vérifie pas un mot de passe dans une requete car il est haché
    // Ok  on connecte l'user
    session_start(); 
    $_SESSION['pseudo'] = $resultat['pseudo']; // Pour le pseudo et id de l'utilisateur.
    $_SESSION['id'] = $resultat['adherentID'];
    $_SESSION['email'] = $resultat['email'];
    $_SESSION['prenom'] = $resultat['prenom'];
    $_SESSION['nom'] = $resultat['nom'];
    //print_r($_SESSION['email']);
    //die();// sa arrete le script
    // echo json_encode(["status" => "ok", "connecté" => true, "pseudo" => $_SESSION['pseudo'], "avatar" => null, "description" => "bien connecté", 'sessionObject' => $_SESSION]);
} else {
    http_response_code(400); 
    echo json_encode(["status" => "ok", "connecté" => false, "description" => "Pseudo ou email ou password incorrect"]);
}
if(isset($_POST['Cookie'])){ // envoie un cookie depuis le serveur vers le navigateur.
    setcookie("email", $_POST['email']);
}header('Location:index.php');
?>