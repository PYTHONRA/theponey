<?php
include("bdd.php");
$req = "SELECT prenom, nom, pseudo, titre, photo FROM adherents LEFT JOIN 
profils ON adherents.adherentID = profils.adherentID"; //C'est une jointure qui prends les champs de deux tables

try { 
    $requetePreparee = $connexion->prepare($req);
    $requetePreparee->execute(); 
    $resultat = $requetePreparee->fetchAll(PDO::FETCH_ASSOC); // un tableau de resultat
}catch (Exception $err) {
    http_response_code(500);
    echo json_encode($err->getMessage());
    exit;
  }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <pre> 
<?php print_r($resultat[0]["pseudo"]); //affiche les resultats de la req sous forme de tableau(multidimensionnel) ?> 
    </pre>
    <?php
    $tableau = [ //tableau associatif + multidimensionnel
        'Anissa' => ['age' => ['fille' => 22]],
        'Nico' => ['age' => 36]
    ];
    echo ($tableau['Anissa']['age']["fille"]);
    ?>
</body>
</html>