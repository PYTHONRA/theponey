<?php
include(header.php);
session_start();
//S'il n'y a pas de session alors on ne va pas sur cette page
  if(!isset($_SESSION['pseudo'])){ 
    header('Location:/frontend/index.html');
    exit; 
  }
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Home</title>
 </head>
 <body>
   <h2>Voici le profil de <?= $_SESSION['pseudo'] ?></h2>

     <a href="deconnexion.php">
     <button>Se déconnecter</button>
    </a>
 <a href="/frontend/accueil.html">
     <button> accéder au site</button>
 </a>
 </body>
 </html>