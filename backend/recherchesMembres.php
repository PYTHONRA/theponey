<?php
include('bdd.php');
//include('header.php');
// $allusers = $bdd->query('SELECT * FROM adherents ORDER BY id DESC');
if(isset($_GET['search']) AND !empty($_GET['search'])){
    $recherche = htmlspecialchars($_GET['search']);
    try{
      $requete = 'SELECT pseudo, email FROM adherents WHERE pseudo LIKE :pseudo';
  $requetePreparee = $connexion->prepare($requete);
  $requetePreparee->bindValue (':pseudo', '%'.$recherche.'%'); //il me met en valeur la concaténation en pourcentage pour n'importe quelle caractere

  $requetePreparee->execute(); 
  $resultats = $requetePreparee->fetchAll(PDO::FETCH_ASSOC);
  //var_dump($resultats);
}catch (Exception $err) {
  http_response_code(500);
  echo json_encode($err->getMessage());
  exit;
}
    // $allusers = $bdd->query('SELECT * FROM adherents WHERE pseudo LIKE "% ,$recherche. :pseudo '%"");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" />
    <title>RECHERCHE UTILISATEURS</title>
</head>
<body>
  <!-- Barre de navigation -->
  <nav>
        <h1>Poney Fringant</h1>
        <div class="onglets">
            <a class="link" href="recherchesMembres.php">
              Membres</a>
            <a class="link" href="profil.php">
                Profil</a>
            <a class="link" href="deconnexion.php">
                Déconnexion</a>
            <form>
                <input type="search" placeholder="Rechercher">
            </form>
        </div>
    </nav>
    <!-- Fin de la barre de navigation -->
    <form method="GET">
        <input type="search" name="search" placholder="Rechercher un utilisateur" autocomplete="off">
        <input type="submit" name="envoyer">
    </form>
    <?php
    // isset c'est pour vérifier si la variable existe
    if(isset($resultats) AND !empty($resultats)){
      ?>
<table>
  <tr>
    <th>
    Voici le pseudo de :
    </th>
    <th>
       Voici l'email de :
    </th>
  </tr>
  <?php 
    
     foreach($resultats as $membre){
    ?> 
  <tr>
    <td>
    <?= $membre['pseudo']; ?>
    </td>
    <td>
    <?= $membre['email']; ?>
    </td>
  </tr>
 <?php }
 ?>
</table>
<?php } else if (isset($resultats) AND empty($resultats)){
  echo '<p>Pas de résultats</p>';
}
 ?>
</body>
</html>