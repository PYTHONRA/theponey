<?php

//pour afficher les erreurs sur le navigateur, à utiliser qu'en phase de développement
include('header.php');
include('bdd.php');
global $connexion; //Connexion de la bdd du fichier bdd.php

$id = $_POST['id'];  // On récupere les données de l'utilisateur.
$prenom = $_POST['prenom'];
$nom = $_POST['nom'];
$pseudo = $_POST['pseudo'];
$password = $_POST['password'];
$email = $_POST['email'];
$numero = $_POST['numero'];
$adresse1 = $_POST['adresse1'];
$codePostal = $_POST['codePostal'];
$ville = $_POST['ville'];
// $dateAdhesion = $_POST['dateAdhesion'];

if(strlen($pseudo) > 25 || strlen($email) > 50) {
    echo "Le pseudo ou le mail est trop long"; 
    exit; 
} // Nombre max de caractères.

$hash = password_hash($password, PASSWORD_DEFAULT); //La fonction password_hash() crée un nouveau hachage en utilisant un algorithm

$rqt = "INSERT INTO adherents 
    (prenom, nom, pseudo, `password`, email, numero, adresse1, codePostal, ville, dateAdhesion)
    VALUES (:prenom, :nom, :pseudo, :password, :email, :numero, :adresse1, :codePostal, :ville, CURRENT_TIME())";  //Ma requete SQL


try {
    $statement = $connexion->prepare($rqt); // une req préparée ou je lis les données inscrit
    $statement->bindParam(':prenom', $prenom);
    $statement->bindParam(':nom', $nom);
    $statement->bindParam(':pseudo', $pseudo);
    $statement->bindParam(':password', $hash);
    $statement->bindParam(':email', $email);
    $statement->bindParam(':numero', $numero);
    $statement->bindParam(':adresse1', $adresse1);
    $statement->bindParam(':codePostal', $codePostal);
    $statement->bindParam(':ville', $ville);
    $statement->execute();

    // Gestion des sessions 
    session_start(); 
    $_SESSION['id'] = $connexion->lastInsertId(); //session qui va avoir toute les données
    $_SESSION['pseudo'] = $pseudo; 
    
    echo '{"status": "ok", "description": "Vous êtes bien inscrit !"}';
    header('Location:/frontend/index.html');
}
catch (Exception $exception) {
    echo json_encode($exception); //erreur dans la console du navigateur
}

?>