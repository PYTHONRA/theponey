<?php
  session_start();  
  //include('header.php');
  include_once('bdd.php');
  global $connexion;
  //S'il n'y a pas de session alors on ne va pas sur cette page
  if(!isset($_SESSION['pseudo'])){ 
    header('Location:/frontend/index.html');
    exit; 
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" />
</head>
<body>
<title>Mon profil</title>
 <head>
 <body>
   <!-- Barre de navigation -->
   <nav>
        <h1>Poney Fringant</h1>
        <div class="onglets">
            <a class="link" href="accueil.html">
              Accueil</a>
            <a class="link" href="recherchesMembres.php">
              Membres</a>
            <a class="link" href="profil.php">
                Profil</a>
            <a class="link" href="deconnexion.php">
                Déconnexion</a>
            <form>
                <input type="search" placeholder="Rechercher">
            </form>
        </div>
    </nav>
    <!-- Fin de la barre de navigation -->
   <h2>Voici le profil de <?= $_SESSION['pseudo']."  ".$_SESSION['nom'];?></h2>
   <div>Quelques informations sur vous : </div>
    <ul>
      <li>Votre id est : <?= $_SESSION['id']; ?></li>
      <li>Votre mail est : <?= $_SESSION['email']; ?></li>
    </ul>
</body>
</html>