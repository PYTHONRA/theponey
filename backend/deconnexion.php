<?php
    // Pour déconnecter l'utilisateur, il faut détruire la session

    if(session_status() != PHP_SESSION_ACTIVE) {
        session_start(); 
    }
        // "Vider" les variable de session
        session_unset(); 
        session_destroy();//"détuire" une session pour se déconnecter 

        header('Location:/frontend/index.html');
?>
